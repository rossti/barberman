$(document).ready(function () {

    $('#callback-form').hide();
    $('.call-done').hide();

    $( ".callback" ).click(function() {

        setTimeout(function(){
            $('#black-screen').fadeIn('slow').show();
            $('#callback-form').fadeIn('slow').show();
        }, 500);

    });

    $('#done').click(function () {
        $('#callback-form').fadeOut('slow').hide();
        $('.call-done').fadeIn('slow').show();
    });

    $('.fa-times').click(function () {
        $('#black-screen').fadeOut('slow').hide();
        $('#callback-form').fadeOut('slow').hide();
        $('.call-done').fadeOut('slow').hide();
    });

    $('#black-screen').click(function () {
        $('#black-screen').fadeOut('slow').hide();
        $('#callback-form').fadeOut('slow').hide();
        $('.call-done').fadeOut('slow').hide();
    });

});
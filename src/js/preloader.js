$(document).ready(function () {
    $(window).on('load', function () {
        var $preloader = $('#preloader'),
            $svg_anm   = $preloader.find('.fa-cog');
        $svg_anm.fadeOut();
        $preloader.delay(500).fadeOut('slow');
    });
});